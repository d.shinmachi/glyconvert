/**
 * 
 */
package org.glycoinfo.convert.kcf;

import static org.junit.Assert.assertEquals;

import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertConfig;
import org.glycoinfo.convert.error.ConvertException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { KcfToGlycoctToWurcsConverterTest.class, GlyConvertConfig.class })
public class KcfToGlycoctToWurcsConverterTest {

	@Autowired
	@Qualifier("glycoctToKcfConverter")
	GlyConvert glycoctToKcfConverter;

	@Autowired
	@Qualifier("kcfToGlycoCTConverter")
	GlyConvert kcfToGlycoCTConverter;

	@Autowired
	@Qualifier("kcfToGlycoCTToWurcsConverter")
	GlyConvert kcfToWurcsConverter;

//	"RES
//1b:x-dman-HEX-1:5 
//2b:a-dman-HEX-1:5 
//3b:a-dman-HEX-1:5 
//4b:a-dman-HEX-1:5 
//5s:anhydro 
//LIN 
//1:1o(1+1)2d 
//2:2o(6+1)3d 
//3:3o(2+1)4d 
//4:1d(2+1)5n 
//5:1o(5+1)5n"
	
	String kcfOnGlytoucan = "ENTRY     XYZ          Glycan\n"
			+ "NODE      5\n"
			+ "          1     GlcNAc     15.0     7.0\n"
			+ "          2     GlcNAc      8.0     7.0\n"
			+ "          3     Man         1.0     7.0\n"
			+ "          4     Man        -6.0    12.0\n"
			+ "          5     Man        -6.0     2.0\n"
			+ "EDGE      4\n"
			+ "          1     2:b1       1:4\n"
			+ "          2     3:b1       2:4\n"
			+ "          3     5:a1       3:3\n"
			+ "          4     4:a1       3:6\n"
			+ "///\n";
	
	String glycoctOnGlytoucan = "RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dglc-HEX-1:5\n"
			+ "4s:n-acetyl\n"
			+ "5b:b-dman-HEX-1:5\n"
			+ "6b:a-dman-HEX-1:5\n"
			+ "7b:a-dman-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n"
			+ "3:3d(2+1)4n\n"
			+ "4:3o(4+1)5d\n"
			+ "5:5o(3+1)6d\n"
			+ "6:5o(6+1)7d\n\n";

    String wurcsSampleOnGlytoucan = "WURCS=2.0/4,5,4/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1";
    
	String str1="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";

	String str3="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n\n";
	String str4="RES\n"
			+ "1b:a-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	String str5="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	
	String kcf2="ENTRY         CT-1             Glycan\\n"
			+ "NODE  2\\n"
			+ "     1  GlcNAc   0   0\\n"
			+ "     2  Gal   -8   0\\n"
			+ "EDGE  1\\n"
			+ "     1  2:b1     1:4\\n"
			+ "///\\n";
	/*
	 * 
	 * ENTRY         CT-1             Glycan
NODE  2
     1  a-L-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///
 * 
	 */
	String kcf3="ENTRY         CT-1             Glycan\r\n"
			+ "NODE  2\r\n"
			+ "     1  D-GlcpNAc   0   0\r\n"
			+ "     2  Gal   -8   0\r\n"
			+ "EDGE  1\r\n"
			+ "     1  2:b1     1:4\r\n"
			+ "///\r\n";
	     
	/*
	 * RES
1b:a-lglc-HEX-1:5
2s:n-acetyl
3b:b-dgal-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(4+1)3d

ENTRY         CT-1             Glycan
NODE  2
     1  a-L-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///

	 */
	
	String kcf4="ENTRY         CT-1             Glycan\\r\\n"
			+ "NODE  2\\r\\n"
			+ "     1  GlcNAc   0   0\\r\\n"
			+ "     2  Gal   -8   0\\r\\n"
			+ "EDGE  1\\r\\n"
			+ "     1  2:b1     1:4\\r\\n"
			+ "///\\r\\n";
	
/*
 * RES
1b:x-dglc-HEX-1:5
2s:n-acetyl
3b:b-dgal-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(4+1)3d

ENTRY         CT-1             Glycan
NODE  2
     1  D-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///

 * 
 */
	
	String kcf5="ENTRY         CT-1             Glycan\\r\\n"
			+ "NODE  2\\r\\n"
			+ "     1  D-GlcpNAc   0   0\\r\\n"
			+ "     2  Gal   -8   0\\r\\n"
			+ "EDGE  1\\r\\n"
			+ "     1  2:b1     1:4\\r\\n"
			+ "///\\r\\n";	
	
//	ENTRY         CT-1             Glycan\nNODE  2\n     1  D-GlcpNAc   0   0\n     2  Gal   -8   0\nEDGE  1\n     1  2:b1     1:4\n///
	
	String glycoct6="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dglc-HEX-1:5\n"
			+ "4s:n-acetyl\n"
			+ "5s:(r)-lactate\n"
			+ "6b:b-dglc-HEX-1:5\n"
			+ "7s:n-acetyl\n"
			+ "8b:b-dglc-HEX-1:5\n"
			+ "9s:n-acetyl\n"
			+ "10s:(r)-lactate\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n"
			+ "3:3d(2+1)4n\n"
			+ "4:3o(3+1)5n\n"
			+ "5:3o(4+1)6d\n"
			+ "6:6d(2+1)7n\n"
			+ "7:6o(4+1)8d\n"
			+ "8:8d(2+1)9n\n"
			+ "9:8o(3+1)10n\n";
	
	String kcf1="ENTRY         CT-1             Glycan\n"
			+ "NODE  2\n"
			+ "     1  D-GlcpNAc   0   0\n"
			+ "     2  Gal   -8   0\n"
			+ "EDGE  1\n"
			+ "     1  2:b1     1:4\n"
			+ "///\n";
	
	String kcf6="ENTRY         CT-1             Glycan\n"
			+ "NODE  6\n"
			+ "     1  GlcNAc   -16   2\n"
			+ "     2  GlcNAc   -24   2\n"
			+ "     3  D-GlcpNAc   0   0\n"
			+ "     4  GlcNAc   -8   0\n"
			+ "     5  (r)-lactate   -16   -2\n"
			+ "     6  (r)-lactate   -32   2\n"
			+ "EDGE  5\n"
			+ "     1  4:b1     3:4\n"
			+ "     2  5:1     4:3\n"
			+ "     3  1:b1     4:4\n"
			+ "     4  2:b1     1:4\n"
			+ "     5  6:1     2:3\n"
			+ "///\n";
	
	String kcf7 = "ENTRY         CT-1             Glycan\n"
			+ "NODE  9\n"
			+ "     1  GlcNAc   -32   0\n"
			+ "     2  GlcNAc   -16   0\n"
			+ "     3  GlcNAc   -48   0\n"
			+ "     4  Gal   -40   0\n"
			+ "     5  Fuc   -56   -2\n"
			+ "     6  Gal   -8   0\n"
			+ "     7  Gal   -24   0\n"
			+ "     8  Glc   0   0\n"
			+ "     9  Gal   -56   2\n"
			+ "EDGE  8\n"
			+ "     1  6:b1     8:4\n"
			+ "     2  5:a1     3:3\n"
			+ "     3  9:b1     3:4\n"
			+ "     4  2:b1     6:3\n"
			+ "     5  7:b1     2:4\n"
			+ "     6  1:b1     7:3\n"
			+ "     7  4:b1     1:4\n"
			+ "     8  3:b1     4:3\n"
			+ "///\n";
	
	String glcoct1 = "RES\n1b:b-dlyx-PEN-1:5\n";
	String glycoctrep = "RES\n1r:r1\nREP\nREP1:2o(4+1)2d=-1--1\nRES\n2b:b-dman-HEX-1:5";
	
	String glycoct2 = "RES\n"
			+ "1b:a-dglc-HEX-1:5\n"
			+ "2b:a-dglc-HEX-1:5\n"
			+ "3b:a-dglc-HEX-1:5\n"
			+ "4b:a-dglc-HEX-1:5\n"
			+ "5b:a-dglc-HEX-1:5\n"
			+ "6b:a-dglc-HEX-1:5\n"
			+ "7b:a-dglc-HEX-1:5\n"
			+ "8s:anhydro\n"
			+ "9s:anhydro\n"
			+ "LIN\n"
			+ "1:1o(4+1)2d\n"
			+ "2:2o(4+1)3d\n"
			+ "3:3o(4+1)4d\n"
			+ "4:4o(4+1)5d\n"
			+ "5:5o(4+1)6d\n"
			+ "6:6o(4+1)7d\n"
			+ "7:6d(3+1)8n\n"
			+ "8:6o(6+1)8n\n"
			+ "9:3d(3+1)9n\n"
			+ "10:3o(6+1)9n\n";
	
	String glycoct1kcf ="ENTRY         CT-1             Glycan\n"
			+ "NODE  1\n"
			+ "     1  b-D-Lyxp      \n"
			+ "EDGE  0\n"
			+ "///\n";

	
	/**
	 * Test method for {@link org.glycoinfo.convert.glycoct.GlycoctToKcfScriptConverter#convert()}.
	 */
	@Test
	public void testBlank() throws Exception {
		String result = kcfToGlycoCTConverter.convert("");
		Assert.assertTrue( result.startsWith("system error: rings returned a non-parsable JSON:>"));
	}

	String str2="RES\\r\\n"
			+ "1b:a-lglc-HEX-1:5\\r\\n"
			+ "2s:n-acetyl\\r\\n"
			+ "3b:b-dgal-HEX-1:5\\r\\n"
			+ "LIN\\r\\n"
			+ "1:1d(2+1)2n\\r\\n"
			+ "2:1o(4+1)3d";
//	@Test  weird test
	public void test1() throws Exception {
		String result = kcfToGlycoCTConverter.convert(glycoct1kcf);
		assertEquals(str2, result);
	}
	
//	@Test weird kcf?
	public void test2() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcf2);
		assertEquals(str2, result);
	}
	
	@Test
	public void test3() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcf3);
		assertEquals(str3, result);
	}
	
//	@Test weird kcf?
	public void test4() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcf4);
		assertEquals(str4, result);
	}
	
//	@Test weird kcf?
	public void test5() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcf5);
		assertEquals(str5, result);
	}

//	@Test weird kcf?
	public void test6() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcfOnGlytoucan);
		assertEquals(glycoct6, result);
	}

	@Test
	public void testGlytoucanSample() throws Exception {
		String result = kcfToGlycoCTConverter.convert(kcfOnGlytoucan);
		assertEquals(glycoctOnGlytoucan, result);
	}

	@Test
	public void testWurcsGlytoucanSample() throws Exception {
		String result = kcfToWurcsConverter.convert(kcfOnGlytoucan);
		assertEquals(wurcsSampleOnGlytoucan, result);
	}


	@Test
	public void testct() throws Exception {
		String result = glycoctToKcfConverter.convert(glcoct1);
		assertEquals(glycoct1kcf, result);
	}

	@Test
	public void testrep() throws Exception {
		String result = glycoctToKcfConverter.convert(glycoctrep);
		assertEquals("ERROR! The repeating unit is unsupported in this tool. (GlycoCTtoKCF) ", result);
	}

//	@Test TODO: known issue
	public void testct2() throws Exception {
		String result = glycoctToKcfConverter.convert(glycoct2);
		assertEquals(kcf6, result);
	}

	@Test
	public void testct3() throws Exception {
//		rings.t.soka.ac.jp/cgi-bin/tools/utilities/convert/convertJson.pl?convert_to=Kcf&in_data=RES\n1b:b-dglc-HEX-1:5\n2b:b-dgal-HEX-1:5\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dgal-HEX-1:5\n6b:b-dglc-HEX-1:5\n7s:n-acetyl\n8b:b-dgal-HEX-1:5\n9b:b-dglc-HEX-1:5\n10s:n-acetyl\n11b:a-lgal-HEX-1:5|6:d\n12b:b-dgal-HEX-1:5\nLIN\n1:1o(4+1)2d\n2:2o(3+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6d(2+1)7n\n7:6o(4+1)8d\n8:8o(3+1)9d\n9:9d(2+1)10n\n10:9o(3+1)11d\n11:9o(4+1)12d\n
//		rings.t.soka.ac.jp/cgi-bin/tools/utilities/convert/convertJson.pl?convert_to=glycoct&in_data=ENTRY     XYZ          Glycan\nNODE      5\n          1     GlcNAc     15.0     7.0\n          2     GlcNAc      8.0     7.0\n          3     Man         1.0     7.0\n          4     Man        -6.0    12.0\n          5     Man        -6.0     2.0\nEDGE      4\n    1     2:b1       1:4\n        2     3:b1       2:4\n         3     5:a1       3:3\n          4     4:a1       3:6\n///&text=json

		String result = glycoctToKcfConverter.convert(str1);
		assertEquals(kcf1, result);
	}
	
	@Test
	public void testct4() throws ConvertException {
//		works:
//		http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/convert/convertJson.pl?convert_to=Kcf&in_data=RES%0A1b:b-dglc-HEX-1:5%0A2b:b-dgal-HEX-1:5%0A3b:b-dglc-HEX-1:5%0A4s:n-acetyl%0A5b:b-dgal-HEX-1:5%0A6b:b-dglc-HEX-1:5%0A7s:n-acetyl%0A8b:b-dgal-HEX-1:5%0A9b:b-dglc-HEX-1:5%0A10s:n-acetyl%0A11b:a-lgal-HEX-1:5%7C6:d%0A12b:b-dgal-HEX-1:5%0ALIN%0A1:1o(4%2B1)2d%0A2:2o(3%2B1)3d%0A3:3d(2%2B1)4n%0A4:3o(4%2B1)5d%0A5:5o(3%2B1)6d%0A6:6d(2%2B1)7n%0A7:6o(4%2B1)8d%0A8:8o(3%2B1)9d%0A9:9d(2%2B1)10n%0A10:9o(3%2B1)11d%0A11:9o(4%2B1)12d%0A
//ET request for "
//		http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/convert/convertJson.pl?convert_to=kcf&in_data=RES%0A1b:b-dglc-HEX-1:5%0A2b:b-dgal-HEX-1:5%0A3b:b-dglc-HEX-1:5%0A4s:n-acetyl%0A5b:b-dgal-HEX-1:5%0A6b:b-dglc-HEX-1:5%0A7s:n-acetyl%0A8b:b-dgal-HEX-1:5%0A9b:b-dglc-HEX-1:5%0A10s:n-acetyl%0A11b:a-lgal-HEX-1:5%7C6:d%0A12b:b-dgal-HEX-1:5%0ALIN%0A1:1o(4%2B1)2d%0A2:2o(3%2B1)3d%0A3:3d(2%2B1)4n%0A4:3o(4%2B1)5d%0A5:5o(3%2B1)6d%0A6:6d(2%2B1)7n%0A7:6o(4%2B1)8d%0A8:8o(3%2B1)9d%0A9:9d(2%2B1)10n%0A10:9o(3%2B1)11d%0A11:9o(4%2B1)12d%0A" resulted in 200 (OK)
		String ct4 = "RES\n1b:b-dglc-HEX-1:5\n2b:b-dgal-HEX-1:5\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dgal-HEX-1:5\n6b:b-dglc-HEX-1:5\n7s:n-acetyl\n8b:b-dgal-HEX-1:5\n9b:b-dglc-HEX-1:5\n10s:n-acetyl\n11b:a-lgal-HEX-1:5|6:d\n12b:b-dgal-HEX-1:5\nLIN\n1:1o(4+1)2d\n2:2o(3+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6d(2+1)7n\n7:6o(4+1)8d\n8:8o(3+1)9d\n9:9d(2+1)10n\n10:9o(3+1)11d\n11:9o(4+1)12d\n";
		String result = glycoctToKcfConverter.convert(ct4);
		assertEquals(kcf7, result);
	}
}