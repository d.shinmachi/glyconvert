/**
 * 
 */
package org.glycoinfo.convert.glycoct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.ConvertTest;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.glycoct.GlycoctToLinearcodeWSConverter;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GlycoctToLinearcodeRingsWSConverterTest.class)
@Configuration
public class GlycoctToLinearcodeRingsWSConverterTest extends ConvertTest {

	Log logger = LogFactory.getLog(GlycoctToLinearcodeRingsWSConverterTest.class);
	
	@Bean
	public GlyConvert getConverter() {
		return new GlycoctToLinearcodeWSConverter();
	}
}
