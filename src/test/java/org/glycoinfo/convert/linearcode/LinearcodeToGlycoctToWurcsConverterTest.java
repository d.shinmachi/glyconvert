/**
 * 
 */
package org.glycoinfo.convert.linearcode;

import static org.junit.Assert.assertEquals;

import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { GlyConvertConfig.class })
public class LinearcodeToGlycoctToWurcsConverterTest {

	@Autowired
	@Qualifier("linearcodeToGlycoCTConverter")
	GlyConvert linearcodeToGlycoCTConverter;

	@Autowired
	@Qualifier("linearcodeToWurcsConverter")
	GlyConvert linearcodeToWurcsConverter;

	String in = "GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN";

	String expected = "RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dglc-HEX-1:5\n"
			+ "4s:n-acetyl\n"
			+ "5b:b-dman-HEX-1:5\n"
			+ "6b:a-dman-HEX-1:5\n"
			+ "7b:b-dglc-HEX-1:5\n"
			+ "8s:n-acetyl\n"
			+ "9b:b-dglc-HEX-1:5\n"
			+ "10s:n-acetyl\n"
			+ "11b:b-dgal-HEX-1:5\n"
			+ "12b:a-dman-HEX-1:5\n"
			+ "13b:b-dglc-HEX-1:5\n"
			+ "14s:n-acetyl\n"
			+ "15b:b-dgal-HEX-1:5\n"
			+ "16b:b-dglc-HEX-1:5\n"
			+ "17s:n-acetyl\n"
			+ "18b:a-lgal-HEX-1:5|6:d\n"
			+ "19b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n"
			+ "3:3d(2+1)4n\n"
			+ "4:3o(4+1)5d\n"
			+ "5:5o(3+1)6d\n"
			+ "6:6o(2+1)7d\n"
			+ "7:7d(2+1)8n\n"
			+ "8:6o(4+1)9d\n"
			+ "9:9d(2+1)10n\n"
			+ "10:9o(4+1)11d\n"
			+ "11:5o(6+1)12d\n"
			+ "12:12o(2+1)13d\n"
			+ "13:13d(2+1)14n\n"
			+ "14:13o(4+1)15d\n"
			+ "15:12o(6+1)16d\n"
			+ "16:16d(2+1)17n\n"
			+ "17:16o(3+1)18d\n"
			+ "18:16o(4+1)19d\n\n";

	@Test
	public void testBlank() throws Exception {
		String result = linearcodeToGlycoCTConverter.convert("");
		Assert.assertTrue( result.startsWith("system error: rings returned a non-parsable JSON:>"));
	}

	@Test
    public void test1() throws Exception {
		String result = linearcodeToGlycoCTConverter.convert(in);
		assertEquals(expected, result);
	}
	
	@Test
    public void test2() throws Exception {
		
		String expected = "WURCS=2.0/6,13,12/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-2-3-4-2-2-5-4-2-5-2-6-5/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_d4-f1_f4-g1_h2-i1_h6-k1_i4-j1_k3-l1_k4-m1";
		String result = linearcodeToWurcsConverter.convert(in);
		assertEquals(expected, result);
	}
}