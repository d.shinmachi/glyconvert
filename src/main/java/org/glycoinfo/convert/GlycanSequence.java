package org.glycoinfo.convert;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GlycanSequence {
	
	String format;
	String structure;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	String message;
	
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getStructure() {
		return structure;
	}
	public void setStructure(String structure) {
		this.structure = structure;
	}
	@Override
	public String toString() {
		return "GlycanSequence [format=" + format + ", structure=" + structure + ", message=" + message + "]";
	}
	
	

}
